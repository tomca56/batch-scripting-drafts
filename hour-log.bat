@echo off
set /p newday=Starting new daily log? [y/n]: 
set /p task=Enter task description...
IF %newday%==y (
	echo %time% :: %task% > hour-log.txt
) ELSE (
	echo %time% :: %task% >> hour-log.txt)