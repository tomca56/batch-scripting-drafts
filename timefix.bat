@echo off
cd /windows/system32
w32tm /unregister
w32tm /register
w32tm /config /syncfromflags:manual /manualpeerlist:time.windows.com
w32tm /config /update
set-timezone "GMT Standard Time"
sfc /scannow
DISM.exe /Online /Cleanup-Image /RestoreHealth
shutdown -r -t 0