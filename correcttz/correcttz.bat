@echo OFF
cd C:\Windows
tzutil /g > tznow.txt
set /p tz=<tznow.txt
del tznow.txt
if NOT "%tz%" == "GMT Standard Time" (
	w32tm /register
	w32TM /config /syncfromflags:manual /manualpeerlist:time.windows.com
	w32tm /config /update
	tzutil /s "GMT STandard Time"
	echo --TIMEZONE CORRECTED
	pause
)

