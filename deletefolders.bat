@echo off
echo.
echo.
echo This script will delete...
echo 1. All "$Win" directories in C:
echo 2. Temporary and cache directories in C:\ and \Users
forfiles /P C:\ /M "$*" /C "cmd /c rmdir /q /s @file" 2>nul
forfiles /P C:\ /M "*cache*" /C "cmd /c rmdir /q /s @file" 2>nul
forfiles /P C:\ /M "t*mp*" /C "cmd /c rmdir /q /s @file" 2>nul
forfiles /P C:\Users /M "t*mp*" /C "cmd /c rmdir /q /s @file" 2>nul
set /p list=3. If you wish to delete any other directories, type them separated by spaces [if not type "no"]:
IF "%list%"==no (
	GOTO :EOF
) ELSE (
for /F "tokens=* delims= " %%i in ("%list%") do (rmdir %%i /q /s))

pause