@echo off
taskkill /f /im teams.exe
rmdir %appdata%\Microsoft\Teams /s /q
mkdir %appdata%\Microsoft\Teams
taskkill /f /im onedrive.exe
rmdir %appdata%\Microsoft\onedrive /s /q
mkdir %appdata%\Microsoft\onedrive
taskkill /f /im outlook.exe
rmdir %appdata%\Microsoft\outlook /s /q
mkdir %appdata%\Microsoft\outlook
cmdkey.exe /list > "%TEMP%\List.txt"
findstr.exe Target "%TEMP%\List.txt" > "%TEMP%\tokensonly.txt"
FOR /F "tokens=1,2 delims= " %%G IN (%TEMP%\tokensonly.txt) DO cmdkey.exe /delete:%%H
del "%TEMP%\List.txt" /s /f /q
del "%TEMP%\tokensonly.txt" /s /f /q
DISM /Online /Cleanup-Image /RestoreHealth
sfc /scannow
gpupdate /force
shutdown /r /t 20
