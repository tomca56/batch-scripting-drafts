@echo off
set /p sec="Choose how long until the computer locks the display > "
powercfg.exe /SETACVALUEINDEX SCHEME_CURRENT SUB_VIDEO VIDEOIDLE %sec%
powercfg.exe /SETACVALUEINDEX SCHEME_CURRENT SUB_VIDEO VIDEOCONLOCK %sec%
powercfg.exe /SETACTIVE SCHEME_CURRENT
