@echo off
set /p direction=scrolling [u]p or [d]own? [u/d]:
IF %direction%==u (
	FOR /L %%i IN (1,1,5000) DO call mouse scrollUp 15000000
)
IF %direction%==d (
	FOR /L %%i IN (1,1,5000) DO call mouse scrollDown 15000000
)