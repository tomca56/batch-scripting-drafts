@echo off
taskkill /f /im onedrive.exe
rmdir %appdata%\Microsoft\onedrive /s /q
mkdir %appdata%\Microsoft\onedrive

cmdkey.exe /list > "%TEMP%\List.txt"
findstr.exe Target "%TEMP%\List.txt" > "%TEMP%\tokensonly.txt"
FOR /F "tokens=1,2 delims= " %%G IN (%TEMP%\tokensonly.txt) DO cmdkey.exe /delete:%%H
del "%TEMP%\List.txt" /s /f /q
del "%TEMP%\tokensonly.txt" /s /f /q

%Systemroot%\SysWOW64\OneDriveSetup.exe /uninstall
winget install microsoft.onedrive
onedrive.reg /q /s
shutdown /r /t 0